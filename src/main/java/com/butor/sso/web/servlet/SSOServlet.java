/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.sso.web.servlet;

/*
 * @author asawan
 */

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.user.User;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.json.JsonHelper;
import org.butor.sso.SSOConstants;
import org.butor.sso.SSOHelper;
import org.butor.sso.SSOInfo;
import org.butor.sso.SSOManager;
import org.butor.sso.TicketInfo;
import org.butor.sso.UserInfoProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.api.client.http.HttpStatusCodes;

public class SSOServlet extends HttpServlet {
	private static final long serialVersionUID = 7691215238073544628L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	protected SSOManager ssoManager = null;
	private JsonHelper jsh = new JsonHelper();
	protected UserInfoProvider userInfoProvider = null;
	protected String ssoSigninUrl = null;

	public void init(ServletConfig config) {
		WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
		ssoManager = ctxt.getBean(SSOManager.class);

		userInfoProvider = ctxt.getBean(UserInfoProvider.class);
		ssoSigninUrl = (String)ctxt.getBean("ssoSigninUrl");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String page = req.getRequestURI().substring(req.getContextPath().length()).toLowerCase();
		if (page != null)
			page = validateStr(page);

		String service = req.getParameter("service");
		if (service != null)
			service = validateStr(service);

		// Log all args
		if (logger.isDebugEnabled()) {
			StringBuilder buff = new StringBuilder();
			String qs = req.getRequestURI();
			buff.append("URI:").append(qs).append(", args:");

			Enumeration<String> attrs = req.getParameterNames();
			while (attrs.hasMoreElements()) {
				String an = attrs.nextElement();
				buff.append(an);
				buff.append("=");
				buff.append(req.getParameter(an));
				if (attrs.hasMoreElements()) {
					buff.append(", ");
				}
			}

			buff.append(", page:").append(page);
			logger.debug(buff.toString());
		}

		if (page.startsWith("/signout")) {
			destroySSO(req, resp, service);
		} else if (page.startsWith("/checksso")) {
			checkSSO(req, resp);
		} else if (page.startsWith("/checkticket")) {
			checkTicket(req, resp);
		} else if (page.startsWith("/getticket")) {
			genTicket(req, resp);
		} else if (page.startsWith("/reset")) {
			resetPwd(req, resp);
		} else if (page.startsWith("/hashtoken")) {
			hashToken(req, resp);
		} else {
			resp.setStatus(HttpStatusCodes.STATUS_CODE_SERVICE_UNAVAILABLE);
		}
	}

	public void checkSSO(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String ssoId = req.getParameter(SSOConstants.SSO_SSO_ID);

		SSOInfo sos = ssoManager.getSSOSession(ssoId);
		if (sos == null) {
			if (logger.isDebugEnabled())
				logger.debug("sso not found {}", ssoId);
			resp.setStatus(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED);
			return;
		}
		if (logger.isDebugEnabled())
			logger.debug("sso is valid {}", ssoId);
		resp.setStatus(HttpStatusCodes.STATUS_CODE_OK);
		answer(req, resp, jsh.serialize(sos));
	}

	public void genTicket(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String ssoId = SSOHelper.getCookie(req, SSOConstants.SSO_SSO_ID);
		String ticket = ssoManager.createTicket(ssoId);
		if (ticket == null) {
			resp.setStatus(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED);
			return;
		}
		resp.setStatus(HttpStatusCodes.STATUS_CODE_OK);
		answer(req, resp, ticket);
	}

	public void checkTicket(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String passToken = req.getParameter(SSOConstants.SSO_TICKET);

		TicketInfo li = ssoManager.validateTicket(passToken);
		if (li == null) {
			resp.setStatus(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED);
			return;
		}
		String so = jsh.serialize(li);
		so = so.replace("\"id\":", "\"user_login\":");
		resp.setStatus(HttpStatusCodes.STATUS_CODE_OK);
		answer(req, resp, so);
	}

	public void hashToken(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String token = req.getParameter("token");
		String hash = CommonChecksumFunction.SHA256.generateChecksum(token);
		String res = String.format("{token:%s, hash:%s}", token, hash);
		resp.setStatus(HttpStatusCodes.STATUS_CODE_OK);
		answer(req, resp, res);
	}

	public void resetPwd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		String token = req.getParameter("t");
		String sessionId = req.getSession().getId();
		String lang = "fr";
		String domain = req.getServerName();
		User user = userInfoProvider.readUser(id, sessionId, null, lang, domain);

		// the reset token was stored in the pwd field
		if (null == token || null == user || !token.equals(user.getPwd())) {
			resp.sendRedirect(ssoSigninUrl +"?m=b" +"&_=" +System.currentTimeMillis());
			return;
		}
		if (isTwoStepsSignin(user.getFirmId(), sessionId, null, lang, domain)) {
			// change pwd and ask a question
			HttpSession hts = req.getSession();
			hts.setAttribute("forceSignin", "1");
		}
		resp.sendRedirect(ssoSigninUrl +"?m=c&id=" +id +"&token=" +token +"&_=" +System.currentTimeMillis());
	}

	protected boolean isTwoStepsSignin(long firmId, String sessionId, String reqId, String lang, String domain) {
		boolean twoStepsSignin = true;
		Firm firm = userInfoProvider.readFirm(firmId, sessionId, reqId,lang, domain);
		if (firm == null) {
			logger.warn("Firm (firmId={}) not found or there is permission problem!", firmId);
		} else if (firm.getAttribute("twoStepsSignin") != null) {
			twoStepsSignin = (Boolean) firm.getAttribute("twoStepsSignin");
			if (!twoStepsSignin) {
				// ignore challenge questions
				logger.info("Firm {} has disabled signin challenge questions!", firm.getFirmName());
			}
		}
		return twoStepsSignin;
	}

	public void destroySSO(HttpServletRequest req, HttpServletResponse resp, String service_) throws ServletException,
			IOException {
		String ssoId = SSOHelper.getCookie(req, SSOConstants.SSO_SSO_ID);
		logger.info(String.format("Destroying sso %s. ...", ssoId));
		SSOHelper.removeCookie(resp, SSOConstants.SSO_SSO_ID, "/");

		HttpSession session = req.getSession();
		session.removeAttribute(SSOConstants.SSO_ID);
		session.removeAttribute(SSOConstants.SSO_SSO_ID);
		session.invalidate();

		ssoManager.destroySSOSession(ssoId);

		resp.setStatus(HttpStatusCodes.STATUS_CODE_OK);
		if (service_ != null) {
			if (service_.indexOf("?") == -1) {
				service_ += "?";
			} else {
				service_ += "&";
			}
			service_ += "_=" +System.currentTimeMillis();
			resp.sendRedirect(service_);
			return;
		}
	}

	private String validateStr(String str_) throws UnsupportedEncodingException {
		// TODO prevent crosssite scripting
		// like: http://localhost:8080/sso/signin?page=%22%22%3E%3Cscript%3Ealert%28%22123%20xss%22%29%3C/script%3E
		if (str_ != null) {
			str_ = URLDecoder.decode(str_, "UTF-8");
			if (str_.indexOf("<") > -1 || str_.indexOf(">") > -1 || str_.indexOf("script") > -1) {
				logger.warn("Got invalid string! str=" + str_);
				return "";
			}
		}
		return str_;
	}

	public void answer(HttpServletRequest req, HttpServletResponse resp, String answer_) throws ServletException,
			IOException {
		resp.setContentType("text/json");
		if (answer_ != null) {
			byte[] bb = answer_.getBytes("utf-8");
			resp.setContentLength(bb.length);
			resp.setCharacterEncoding("utf-8");
			resp.getOutputStream().write(bb);
			resp.flushBuffer();
		}
	}

	public String getUsername(HttpServletRequest req) {
		String username = null;
		if (req != null) {
			username = req.getRemoteUser();
		}
		return username;
	}
	final class ByteCountingOutputStream extends OutputStream {

		  /** Number of bytes written. */
		  long count;

		  @Override
		  public void write(byte[] b, int off, int len) throws IOException {
		    count += len;
		  }

		  @Override
		  public void write(int b) throws IOException {
		    count++;
		  }
		}

}
