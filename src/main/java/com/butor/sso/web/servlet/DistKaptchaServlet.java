/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package com.butor.sso.web.servlet;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.sso.SSOHelper;
import org.butor.sso.SSOInfo;
import org.butor.sso.SSOManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.servlet.KaptchaServlet;
import com.google.code.kaptcha.util.Config;

/**
 * Distributed Kaptcha.
 * Store generated Kaptcha in distributed sso manager so we can verify it later on any sso instance
 * 
 * @author asawan
 *
 */
@SuppressWarnings("serial")
public class DistKaptchaServlet extends KaptchaServlet {
	private Logger logger = LoggerFactory.getLogger(getClass());
	protected SSOManager ssoManager = null;

	private Properties props = new Properties();
	private Producer kaptchaProducer = null;
	private String sessionKeyValue = null;
	private String sessionKeyDateValue = null;

	@Override
	public void init(ServletConfig conf) throws ServletException {
		super.init(conf);

		WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(conf.getServletContext());
		ssoManager = ctxt.getBean(SSOManager.class);


		// Switch off disk based caching.
		ImageIO.setUseCache(false);

		Enumeration<?> initParams = conf.getInitParameterNames();
		while (initParams.hasMoreElements())
		{
			String key = (String) initParams.nextElement();
			String value = conf.getInitParameter(key);
			this.props.put(key, value);
		}

		Config config = new Config(this.props);
		this.kaptchaProducer = config.getProducerImpl();
		this.sessionKeyValue = config.getSessionKey();
		this.sessionKeyDateValue = config.getSessionDate();
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// store the text in the session
		String kapSsoId = SSOHelper.getCookie(req, "kap");
		ssoManager.destroySSOSession(kapSsoId);

		// CANNOT CALL SUPER!
		// WE NEED TO SET COOKIE BEFORE WRITING THE RESPONSE!
		
		//super.doGet(req, resp);
		/* super code --------------------- */
		// Set standard HTTP/1.1 no-cache headers.
		resp.setHeader("Cache-Control", "no-store, no-cache");

		// return a jpeg
		resp.setContentType("image/jpeg");

		// create the text for the image
		String capText = this.kaptchaProducer.createText();
		/* -------------------------------- */
		
		// update cap in tempo sso info
		SSOInfo si = ssoManager.createSSOSession(capText, "", "", "", "");
		// set cookie
		SSOHelper.setCookie(resp, "kap", si.getSsoId(), 60, "/sso");

		/* -------------------------------- */
		// create the image with the text
		BufferedImage bi = this.kaptchaProducer.createImage(capText);

		ServletOutputStream out = resp.getOutputStream();

		// write the data out
		ImageIO.write(bi, "jpg", out);

		// fixes issue #69: set the attributes after we write the image in case the image writing fails.

		// store the text in the session
		req.getSession().setAttribute(this.sessionKeyValue, capText);

		// store the date in the session so that it can be compared
		// against to make sure someone hasn't taken too long to enter
		// their kaptcha
		req.getSession().setAttribute(this.sessionKeyDateValue, new Date());		/* -------------------------------- */
		
		String sessionId = req.getParameter("si");
		logger.info("Generated kaptcha={} for sessionId={}", capText, sessionId);
	}
}
