var App = {
	"ready": function() {},
	"pushHistory": function() {},
	"createMsgPanel": function() {},
	"createBottomPanel": function() {},
	"createBackToTopLink": function() {},
	"mask": function() {},
	"unmask": function() {},
	"init": function() {},
	"getId": function() {},
	"getSessionId": function() {},
	"setSessionId": function() {},
	"scrollTo": function() {},
	"showStatus": function() {},
	"progress": function() {},
	"removeStatus": function() {},
	"showMsg": function() {},
	"hideMsg": function() {},
	"error": function() {},
	"info": function() {},
	"setLang": function() {},
	"switchLang": function() {},
	"translate": function() {},
	"translateElem": function() {},
	"tr": function() {},
	"Bundle": {
		"getLang": function() {},
		"setLang": function() {},
		"add": function() {},
		"remove": function() {},
		"get": function() {}
	},
	"Utils": {
		"leftPad": function() {},
		"formatDateTime": function() {},
		"formatDate": function() {},
		"parseDate": function() {},
		"getUUID": function() {},
		"isEmpty": function() {},
		"enable": function() {},
		"disable": function() {},
		"selectNavItem": function() {},
		"fillSelect": function() {},
		"formatCurrency": function() {},
		"formatInt": function() {},
		"formatYield": function() {},
		"formatPrice": function() {},
		"formatNumber": function() {}
	},
	"AttrSet": {
		"getCodeSet": function() {},
		"getCodeSets": function() {},
		"createHelper": function() {}
	},
	"augment": function() {},
	"getLang": function() {},
	"isEmpty": function() {},
	"formatDate": function() {},
	"parseDate": function() {},
	"events": {
		"bind": function() {},
		"unbind": function() {},
		"fire": function() {}
	},
	"dlg": {
		"registry": {
		},
		"getDlg": function() {},
		"openDlg": function() {}
	},
	"loadScripts": function() {},
	"TABS": function() {},
	"genExterns": function() {},
	"genExports": function() {},
	"externAll": function() {},
	"exportAll": function() {},
	"signInOut": function() {},
	"getSessionInfo": function() {},
	"handleSesssionInfo": function() {},
	"renewKaptcha": function() {},
	"cancel": function() {},
	"back": function() {},
	"showPage": function() {},
	"signInS1": function() {},
	"signInS2": function() {},
	"setQRs": function() {},
	"changePwd": function() {},
	"resetPwd": function() {},
	"getUserInfo": function() {},
	"getS2Info": function() {},
	"signInP": function() {},
	"resetPwdP": function() {},
	"changePwdP": function() {},
	"activateP": function() {}
};
var AJAX = {
	"call": function() {},
	"streamingReq": function() {},
	"onNotification": function() {}
};
var LOGGER = {
	"INFO": function() {},
	"WARN": function() {},
	"ERROR": function() {},
	"DISABLED": function() {},
	"infoEnabled": function() {},
	"warnEnabled": function() {},
	"errorEnabled": function() {},
	"setLevel": function() {},
	"info": function() {},
	"warn": function() {},
	"error": function() {}
};
var jQuery = {
	"fn": {
		"constructor": function() {},
		"init": function() {},
		"selector": function() {},
		"jquery": function() {},
		"length": function() {},
		"size": function() {},
		"toArray": function() {},
		"get": function() {},
		"pushStack": function() {},
		"each": function() {},
		"ready": function() {},
		"eq": function() {},
		"first": function() {},
		"last": function() {},
		"slice": function() {},
		"map": function() {},
		"end": function() {},
		"push": function() {},
		"sort": function() {},
		"splice": function() {},
		"extend": function() {},
		"data": function() {},
		"removeData": function() {},
		"queue": function() {},
		"dequeue": function() {},
		"delay": function() {},
		"clearQueue": function() {},
		"promise": function() {},
		"attr": function() {},
		"removeAttr": function() {},
		"prop": function() {},
		"removeProp": function() {},
		"addClass": function() {},
		"removeClass": function() {},
		"toggleClass": function() {},
		"hasClass": function() {},
		"val": function() {},
		"on": function() {},
		"one": function() {},
		"off": function() {},
		"bind": function() {},
		"unbind": function() {},
		"live": function() {},
		"die": function() {},
		"delegate": function() {},
		"undelegate": function() {},
		"trigger": function() {},
		"triggerHandler": function() {},
		"toggle": function() {},
		"hover": function() {},
		"blur": function() {},
		"focus": function() {},
		"focusin": function() {},
		"focusout": function() {},
		"load": function() {},
		"resize": function() {},
		"scroll": function() {},
		"unload": function() {},
		"click": function() {},
		"dblclick": function() {},
		"mousedown": function() {},
		"mouseup": function() {},
		"mousemove": function() {},
		"mouseover": function() {},
		"mouseout": function() {},
		"mouseenter": function() {},
		"mouseleave": function() {},
		"change": function() {},
		"select": function() {},
		"submit": function() {},
		"keydown": function() {},
		"keypress": function() {},
		"keyup": function() {},
		"error": function() {},
		"contextmenu": function() {},
		"find": function() {},
		"has": function() {},
		"not": function() {},
		"filter": function() {},
		"is": function() {},
		"closest": function() {},
		"index": function() {},
		"add": function() {},
		"andSelf": function() {},
		"parent": function() {},
		"parents": function() {},
		"parentsUntil": function() {},
		"next": function() {},
		"prev": function() {},
		"nextAll": function() {},
		"prevAll": function() {},
		"nextUntil": function() {},
		"prevUntil": function() {},
		"siblings": function() {},
		"children": function() {},
		"contents": function() {},
		"text": function() {},
		"wrapAll": function() {},
		"wrapInner": function() {},
		"wrap": function() {},
		"unwrap": function() {},
		"append": function() {},
		"prepend": function() {},
		"before": function() {},
		"after": function() {},
		"remove": function() {},
		"empty": function() {},
		"clone": function() {},
		"html": function() {},
		"replaceWith": function() {},
		"detach": function() {},
		"domManip": function() {},
		"appendTo": function() {},
		"prependTo": function() {},
		"insertBefore": function() {},
		"insertAfter": function() {},
		"replaceAll": function() {},
		"css": function() {},
		"serialize": function() {},
		"serializeArray": function() {},
		"ajaxStart": function() {},
		"ajaxStop": function() {},
		"ajaxComplete": function() {},
		"ajaxError": function() {},
		"ajaxSuccess": function() {},
		"ajaxSend": function() {},
		"show": function() {},
		"hide": function() {},
		"_toggle": function() {},
		"fadeTo": function() {},
		"animate": function() {},
		"stop": function() {},
		"slideDown": function() {},
		"slideUp": function() {},
		"slideToggle": function() {},
		"fadeIn": function() {},
		"fadeOut": function() {},
		"fadeToggle": function() {},
		"offset": function() {},
		"position": function() {},
		"offsetParent": function() {},
		"scrollLeft": function() {},
		"scrollTop": function() {},
		"innerHeight": function() {},
		"outerHeight": function() {},
		"height": function() {},
		"innerWidth": function() {},
		"outerWidth": function() {},
		"width": function() {},
		"alert": function() {},
		"button": function() {},
		"carousel": function() {},
		"collapse": function() {},
		"dropdown": function() {},
		"modal": function() {},
		"tooltip": function() {},
		"popover": function() {},
		"scrollspy": function() {},
		"tab": function() {},
		"typeahead": function() {},
		"affix": function() {},
		"mask": function() {},
		"unmask": function() {},
		"isMasked": function() {}
	},
	"extend": function() {},
	"noConflict": function() {},
	"isReady": function() {},
	"readyWait": function() {},
	"holdReady": function() {},
	"ready": function() {},
	"bindReady": function() {},
	"isFunction": function() {},
	"isArray": function() {},
	"isWindow": function() {},
	"isNumeric": function() {},
	"type": function() {},
	"isPlainObject": function() {},
	"isEmptyObject": function() {},
	"error": function() {},
	"parseJSON": function() {},
	"parseXML": function() {},
	"noop": function() {},
	"globalEval": function() {},
	"camelCase": function() {},
	"nodeName": function() {},
	"each": function() {},
	"trim": function() {},
	"makeArray": function() {},
	"inArray": function() {},
	"merge": function() {},
	"grep": function() {},
	"map": function() {},
	"guid": function() {},
	"proxy": function() {},
	"access": function() {},
	"now": function() {},
	"uaMatch": function() {},
	"sub": function() {},
	"browser": {
		"webkit": function() {},
		"version": function() {},
		"safari": function() {}
	},
	"Callbacks": function() {},
	"Deferred": function() {},
	"when": function() {},
	"boxModel": function() {},
	"support": {
		"leadingWhitespace": function() {},
		"tbody": function() {},
		"htmlSerialize": function() {},
		"style": function() {},
		"hrefNormalized": function() {},
		"opacity": function() {},
		"cssFloat": function() {},
		"checkOn": function() {},
		"optSelected": function() {},
		"getSetAttribute": function() {},
		"enctype": function() {},
		"html5Clone": function() {},
		"submitBubbles": function() {},
		"changeBubbles": function() {},
		"focusinBubbles": function() {},
		"deleteExpando": function() {},
		"noCloneEvent": function() {},
		"inlineBlockNeedsLayout": function() {},
		"shrinkWrapBlocks": function() {},
		"reliableMarginRight": function() {},
		"pixelMargin": function() {},
		"boxModel": function() {},
		"noCloneChecked": function() {},
		"optDisabled": function() {},
		"radioValue": function() {},
		"checkClone": function() {},
		"appendChecked": function() {},
		"ajax": function() {},
		"cors": function() {},
		"reliableHiddenOffsets": function() {},
		"doesNotAddBorder": function() {},
		"doesAddBorderForTableAndCells": function() {},
		"fixedPosition": function() {},
		"subtractsBorderForOverflowNotVisible": function() {},
		"doesNotIncludeMarginInBodyOffset": function() {},
		"transition": {
			"end": function() {}
		}
	},
	"cache": {
		"1": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"2": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"1": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"2": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"3": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"4": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"5": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"6": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"7": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"8": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"undefined": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"keydown": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"focusin": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"3": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"4": {
			"olddisplay": function() {},
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"5": {
			"olddisplay": function() {}
		},
		"6": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"7": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"8": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"9": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"10": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"11": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"12": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"13": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"14": {
			"events": {
				"keypress": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"data": {
						},
						"handler": function() {},
						"guid": function() {},
						"selector": {
						},
						"quick": {
						},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		}
	},
	"uuid": function() {},
	"expando": function() {},
	"noData": {
		"embed": function() {},
		"object": function() {},
		"applet": function() {}
	},
	"hasData": function() {},
	"data": function() {},
	"removeData": function() {},
	"_data": function() {},
	"acceptData": function() {},
	"_mark": function() {},
	"_unmark": function() {},
	"queue": function() {},
	"dequeue": function() {},
	"valHooks": {
		"option": {
			"get": function() {}
		},
		"select": {
			"get": function() {},
			"set": function() {}
		},
		"radio": {
			"set": function() {}
		},
		"checkbox": {
			"set": function() {}
		}
	},
	"attrFn": {
		"val": function() {},
		"css": function() {},
		"html": function() {},
		"text": function() {},
		"data": function() {},
		"width": function() {},
		"height": function() {},
		"offset": function() {},
		"blur": function() {},
		"focus": function() {},
		"focusin": function() {},
		"focusout": function() {},
		"load": function() {},
		"resize": function() {},
		"scroll": function() {},
		"unload": function() {},
		"click": function() {},
		"dblclick": function() {},
		"mousedown": function() {},
		"mouseup": function() {},
		"mousemove": function() {},
		"mouseover": function() {},
		"mouseout": function() {},
		"mouseenter": function() {},
		"mouseleave": function() {},
		"change": function() {},
		"select": function() {},
		"submit": function() {},
		"keydown": function() {},
		"keypress": function() {},
		"keyup": function() {},
		"error": function() {},
		"contextmenu": function() {}
	},
	"attr": function() {},
	"removeAttr": function() {},
	"attrHooks": {
		"type": {
			"set": function() {}
		},
		"value": {
			"get": function() {},
			"set": function() {}
		},
		"tabindex": {
			"get": function() {}
		}
	},
	"propFix": {
		"tabindex": function() {},
		"readonly": function() {},
		"for": function() {},
		"class": function() {},
		"maxlength": function() {},
		"cellspacing": function() {},
		"cellpadding": function() {},
		"rowspan": function() {},
		"colspan": function() {},
		"usemap": function() {},
		"frameborder": function() {},
		"contenteditable": function() {}
	},
	"prop": function() {},
	"propHooks": {
		"tabIndex": {
			"get": function() {}
		}
	},
	"event": {
		"add": function() {},
		"global": {
			"click": function() {},
			"undefined": function() {},
			"keydown": function() {},
			"load": function() {},
			"focusin": function() {},
			"popstate": function() {},
			"beforeunload": function() {},
			"unload": function() {},
			"hashchange": function() {},
			"scroll": function() {},
			"statechange": function() {},
			"keypress": function() {}
		},
		"remove": function() {},
		"customEvent": {
			"getData": function() {},
			"setData": function() {},
			"changeData": function() {}
		},
		"trigger": function() {},
		"dispatch": function() {},
		"props": {
			"0": function() {},
			"1": function() {},
			"2": function() {},
			"3": function() {},
			"4": function() {},
			"5": function() {},
			"6": function() {},
			"7": function() {},
			"8": function() {},
			"9": function() {},
			"10": function() {},
			"11": function() {},
			"12": function() {},
			"13": function() {},
			"14": function() {},
			"15": function() {},
			"16": function() {}
		},
		"fixHooks": {
			"click": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"dblclick": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mousedown": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mouseup": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mousemove": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mouseover": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mouseout": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mouseenter": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"mouseleave": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"keydown": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {}
				},
				"filter": function() {}
			},
			"keypress": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {}
				},
				"filter": function() {}
			},
			"keyup": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {}
				},
				"filter": function() {}
			},
			"contextmenu": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			}
		},
		"keyHooks": {
			"props": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {}
			},
			"filter": function() {}
		},
		"mouseHooks": {
			"props": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {},
				"7": function() {},
				"8": function() {},
				"9": function() {},
				"10": function() {},
				"11": function() {}
			},
			"filter": function() {}
		},
		"fix": function() {},
		"special": {
			"ready": {
				"setup": function() {}
			},
			"load": {
				"noBubble": function() {}
			},
			"focus": {
				"delegateType": function() {}
			},
			"blur": {
				"delegateType": function() {}
			},
			"beforeunload": {
				"setup": function() {},
				"teardown": function() {}
			},
			"mouseenter": {
				"delegateType": function() {},
				"bindType": function() {},
				"handle": function() {}
			},
			"mouseleave": {
				"delegateType": function() {},
				"bindType": function() {},
				"handle": function() {}
			},
			"focusin": {
				"setup": function() {},
				"teardown": function() {}
			},
			"focusout": {
				"setup": function() {},
				"teardown": function() {}
			}
		},
		"simulate": function() {},
		"handle": function() {}
	},
	"removeEvent": function() {},
	"Event": function() {},
	"find": function() {},
	"expr": {
		"order": {
			"0": function() {},
			"1": function() {},
			"2": function() {},
			"3": function() {}
		},
		"match": {
			"ID": {
			},
			"CLASS": {
			},
			"NAME": {
			},
			"ATTR": {
			},
			"TAG": {
			},
			"CHILD": {
			},
			"POS": {
			},
			"PSEUDO": {
			},
			"globalPOS": {
			}
		},
		"leftMatch": {
			"ID": {
			},
			"CLASS": {
			},
			"NAME": {
			},
			"ATTR": {
			},
			"TAG": {
			},
			"CHILD": {
			},
			"POS": {
			},
			"PSEUDO": {
			}
		},
		"attrMap": {
		},
		"attrHandle": {
			"href": function() {},
			"type": function() {}
		},
		"relative": {
			"+": function() {},
			">": function() {},
			"": function() {},
			"~": function() {}
		},
		"find": {
			"ID": function() {},
			"NAME": function() {},
			"TAG": function() {},
			"CLASS": function() {}
		},
		"preFilter": {
			"CLASS": function() {},
			"ID": function() {},
			"TAG": function() {},
			"CHILD": function() {},
			"ATTR": function() {},
			"PSEUDO": function() {},
			"POS": function() {}
		},
		"filters": {
			"enabled": function() {},
			"disabled": function() {},
			"checked": function() {},
			"selected": function() {},
			"parent": function() {},
			"empty": function() {},
			"has": function() {},
			"header": function() {},
			"text": function() {},
			"radio": function() {},
			"checkbox": function() {},
			"file": function() {},
			"password": function() {},
			"submit": function() {},
			"image": function() {},
			"reset": function() {},
			"button": function() {},
			"input": function() {},
			"focus": function() {},
			"hidden": function() {},
			"visible": function() {},
			"animated": function() {}
		},
		"setFilters": {
			"first": function() {},
			"last": function() {},
			"even": function() {},
			"odd": function() {},
			"lt": function() {},
			"gt": function() {},
			"nth": function() {},
			"eq": function() {}
		},
		"filter": {
			"PSEUDO": function() {},
			"CHILD": function() {},
			"ID": function() {},
			"TAG": function() {},
			"CLASS": function() {},
			"ATTR": function() {},
			"POS": function() {}
		},
		":": {
			"enabled": function() {},
			"disabled": function() {},
			"checked": function() {},
			"selected": function() {},
			"parent": function() {},
			"empty": function() {},
			"has": function() {},
			"header": function() {},
			"text": function() {},
			"radio": function() {},
			"checkbox": function() {},
			"file": function() {},
			"password": function() {},
			"submit": function() {},
			"image": function() {},
			"reset": function() {},
			"button": function() {},
			"input": function() {},
			"focus": function() {},
			"hidden": function() {},
			"visible": function() {},
			"animated": function() {}
		}
	},
	"unique": function() {},
	"text": function() {},
	"isXMLDoc": function() {},
	"contains": function() {},
	"filter": function() {},
	"dir": function() {},
	"nth": function() {},
	"sibling": function() {},
	"buildFragment": function() {},
	"clone": function() {},
	"clean": function() {},
	"cleanData": function() {},
	"cssHooks": {
		"opacity": {
			"get": function() {}
		},
		"height": {
			"get": function() {},
			"set": function() {}
		},
		"width": {
			"get": function() {},
			"set": function() {}
		},
		"margin": {
			"expand": function() {}
		},
		"padding": {
			"expand": function() {}
		},
		"borderWidth": {
			"expand": function() {}
		}
	},
	"cssNumber": {
		"fillOpacity": function() {},
		"fontWeight": function() {},
		"lineHeight": function() {},
		"opacity": function() {},
		"orphans": function() {},
		"widows": function() {},
		"zIndex": function() {},
		"zoom": function() {}
	},
	"cssProps": {
		"float": function() {}
	},
	"style": function() {},
	"css": function() {},
	"swap": function() {},
	"curCSS": function() {},
	"get": function() {},
	"post": function() {},
	"getScript": function() {},
	"getJSON": function() {},
	"ajaxSetup": function() {},
	"ajaxSettings": {
		"url": function() {},
		"isLocal": function() {},
		"global": function() {},
		"type": function() {},
		"contentType": function() {},
		"processData": function() {},
		"async": function() {},
		"accepts": {
			"xml": function() {},
			"html": function() {},
			"text": function() {},
			"json": function() {},
			"*": function() {},
			"script": function() {}
		},
		"contents": {
			"xml": {
			},
			"html": {
			},
			"json": {
			},
			"script": {
			}
		},
		"responseFields": {
			"xml": function() {},
			"text": function() {}
		},
		"converters": {
			"* text": function() {},
			"text html": function() {},
			"text json": function() {},
			"text xml": function() {},
			"text script": function() {}
		},
		"flatOptions": {
			"context": function() {},
			"url": function() {}
		},
		"jsonp": function() {},
		"jsonpCallback": function() {},
		"xhr": function() {},
		"cache": function() {}
	},
	"ajaxPrefilter": function() {},
	"ajaxTransport": function() {},
	"ajax": function() {},
	"param": function() {},
	"active": function() {},
	"lastModified": {
	},
	"etag": {
	},
	"speed": function() {},
	"easing": {
		"linear": function() {},
		"swing": function() {}
	},
	"timers": {
	},
	"fx": function() {},
	"offset": {
		"bodyOffset": function() {},
		"setOffset": function() {}
	},
	"cookie": function() {},
	"removeCookie": function() {},
	"maskElement": function() {},
	"unmaskElement": function() {},
	"QueryString": {
	}
};
