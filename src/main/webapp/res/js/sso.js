/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.sso = butor.sso || {};
butor.sso.App = butor.sso.App || butor.App.extend({
	_brand : null,
	_resetPwdLink : null,
	_signInOutLink : null,
	_manageApiKeyLink : null,
	_changePwdLink : null,
	_setQRsLink : null,
	_registerLink : null,

	_content : null,
	_frags : null,
	_service : null,
	_kaptchaImg : null,
	_kaptcha : null,
	_email : null,
	_firstName : null,
	_lastName : null,
	_password : null,
	_newPassword : null,
	_newPassword2 : null,
	_rememberMe : null,
	_token : null,
	
	_avatars : [],
	_avIndex : -1,
	_q1 : null,
	_r1 : null,
	_q2 : null,
	_r2 : null,
	_q3 : null,
	_r3 : null,
	
	_attrSet : new butor.AttrSet(),	
	isEmpty : function(str) {
		return butor.Utils.isEmpty(str);
	},
	construct : function() {
		this.base();
		this._brand = $("#_brand");
		this._manageApiKeyLink = $("#manageApiKeyLink");
		this._resetPwdLink = $("#resetPwdLink");
		this._changePwdLink = $("#changePwdLink");
		this._signInOutLink = $("#signInOutLink");
		this._setQRsLink = $("#setQRsLink");
		this._registerLink = $("#registerLink");
		this._switchLangLink = $("#switchLangLink");
	},
	getId : function() {
		return 'sso';
	},
	start : function() {
		this.base();
		var lang = $.cookie("lang");
		this.setLang(lang);

		this.translate();

		var url = this.tr('contact-us-url');
		if (url != 'contact-us-url') {
			this._contactUsLink = $("#contactUsLink").show();
			this._contactUsLink.click($.proxy(this._contactUs, this));
		}
		$(".copyright").html(this.tr('copyright'));

		this._switchLangLink.click($.proxy(function(e) {this.switchLang()}, this));

		this._brand.click($.proxy(this.cancel, this));
		this._resetPwdLink.click($.proxy(this._showResetPwd, this));
		this._signInOutLink.click($.proxy(this._signInOut, this));
		this._manageApiKeyLink.click($.proxy(this._showManageApiKey, this));
		this._changePwdLink.click($.proxy(this._showChangePwd, this));
		this._content = $('#_content');
		this._service = $.QueryString["service"];
		if (this._service && this._service.indexOf('/') == 0) {
			if (this._service.indexOf('?') == -1) {
				this._service += '?_=' +(new Date()).getTime(); 
			} else {
				this._service += '&' +(new Date()).getTime(); 
			}

			this._service += document.location.hash;
			var slc = this._service.toLowerCase();
			if (slc.indexOf('script')>-1 || slc.indexOf('://')>-1) {
				this._service = null;
			} else {
				this._service = encodeURIComponent(this._service);
			}
		} else {
			this._service = null;
		}
		
		this._token = $.QueryString["token"];
		
		this._frags = $('<div style="display:none">');
		this._getSessionInfo();
		
		App.bind('langChanged', $.proxy(this._langChanged, this));
	},
	_langChanged : function() {
		this._signInOutLink.find('.bundle').html(this._sessionInfo ? this.tr('Sign out') : this.tr('Sign in'));
		this._email && this._email.attr("placeholder", this.tr('ID or Email'));
		this._password && this._password.attr("placeholder", this.tr('Password'));
		this._kaptcha && this._kaptcha.attr("placeholder", this.tr('Type the text you see in the image'));
		
		this._setPwdPopover();
	},
	_loadFrag : function(fragId) {
		var dfr =  $.Deferred();
		var self = this;
		if (this._frags.find('#' +fragId).length == 0) {
			var fragJqe = $('<div>').load(fragId+'.html', function() {
				LOGGER.info('frag ' +fragId +' loaded');
				fragJqe.appendTo(self._frags);
				dfr.resolve();
			}, function() {
				dfr.reject();
			});
		} else {
			dfr.resolve();
		}
		return dfr.promise();
	},
	setLang : function(lang) {
		LOGGER.info('Setting language to ' + lang + ' ...');
		this._switchLangLink.html(
				'<i class="fa fa-language"></i> '
				+ (lang === 'fr' ? 'English' : 'Français'));

		this._bundle.setLang(lang);
	},
	_delayFocus : function(jqe) {
		setTimeout(function() {
			jqe.focus();
			}, 100);
	},
	_signInOut : function() {
		if (this._sessionInfo != null)
			if (this._service)
				window.location='signout?service=' +this._service;
			else
				window.location='signout?service=index.html';
		else
			this._showSignIn();
	},
	_getSessionInfo : function() {
		this._sessionInfo = null;
		var sireqid = butor.sso.SsoAjax.getUserInfo({'scope':this, 'callback' : function(result) {
				if (AJAX.isSuccessWithData(result, sireqid)) {
					var info = result['data'][0];
					this._env = info['env'];
					App.addBundle('common', {
						'pwdRulesPopoverMsg': {'fr' : info['pwdRulesPopoverMsgFr'],
							'en' : info['pwdRulesPopoverMsgEn']}});
					this._sessionInfo = info['user'];
				}
	
				if (this._env !== 'prod') {
					$("#_footer .env").html('/' +this._env);
				}
	
				this._changePwdLink.parent().show();
				if (this._sessionInfo !== undefined) {
					this._signInOutLink.find('.bundle').html(this.tr('Sign out'));
					if (info['twoStepsSignin']) {
						this._setQRsLink.click($.proxy(this._showSetQRs, this)).parent().show();
					}
					this._registerLink.parent().hide();
					this._resetPwdLink.parent().hide();
				} else {
					this._setQRsLink.parent().hide();
					this._registerLink.click($.proxy(this._showRegister, this)).parent().show();
					this._resetPwdLink.parent().show();
				}
	
				this._postInit();

				var m = $.QueryString["m"];
				if (m == "c" && (this._sessionInfo != null || this._token != null))
					this._showChangePwd();
				else if (m == "q" && info['twoStepsSignin'] && this._sessionInfo != null)
					this._showSetQRs();
				else if (m == "r" && this._sessionInfo == null)
					this._showRegister();
				else if (m == "f" && this._sessionInfo == null)
					this._showResetPwd();
				else if (m == "b")
					this._showBadToken();
				else if (m == "i")
					this._showSignedIn();
				else if (m == "a" && this._token != null)
					this._showActivateLogin();
				else if (m == "k" && (this._sessionInfo != null || this._token != null))
					this._showManageApiKey();
				else {
					this.goHome();
				}
			}
		});
		return sireqid;
	},
	goHome : function() {
		if (this._sessionInfo != null)
			this._showSignedIn();
		else
			this._showSignIn();
	},
	_postInit : function() {
		if (this._options.headerVisibleAtStartup == true) {
			$('#_header').fadeIn();
		}
		if (this._options.contentVisibleAtStartup == true) {
			$('#_content').fadeIn();
		}
		if (this._options.footerVisibleAtStartup == true) {
			$('#_footer').fadeIn();
		}
	},
	_showRegister : function() {
		this.hideMsg();
		$.when(this._loadFrag('register')).
		always($.proxy(function() {
			var jqe = this._frags.find('#register').clone();
			this._showContent('register', jqe);
			this._kaptchaImg = jqe.find('#kaptchaImg');
			this._kaptcha = jqe.find('#kaptcha');
			this._email = jqe.find("#email");
			this._password = jqe.find("#password");
			this._firstName = jqe.find("#firstName");
			this._lastName = jqe.find("#lastName");
			this._password2 = jqe.find("#password2");

			this._langChanged();
			
			var val = $.cookie("rememberMe");
			if (!this.isEmpty(val)) {
				this._email.val($.cookie("email"));
			}

			var renewKaptchaP = $.proxy(this.renewKaptcha, this);
			this._kaptchaImg.click(renewKaptchaP);
			jqe.find("#renewKaptcha").click(renewKaptchaP);
			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));
			var registerP = $.proxy(this.register, this);
			jqe.find("#registerBtn").click(registerP);
			jqe.keypress(function(e) {
				if (e.keyCode == '13') {
					registerP();
				}
			});
			this.renewKaptcha();
			this._delayFocus(this._email);
		}, this));
	},
	_showResetPwd : function() {
		this.hideMsg();
		this.hideMsg();
		$.when(this._loadFrag('resetPwd')).
		always($.proxy(function() {
			var jqe = this._frags.find('#resetPwd').clone();
			this._showContent('resetPwd', jqe);
			this._kaptchaImg = jqe.find('#kaptchaImg');
			this._kaptcha = jqe.find('#kaptcha');
			this._email = jqe.find("#email");
			this._password = jqe.find("#password");

			this._langChanged();
			
			var val = $.cookie("rememberMe");
			if (!this.isEmpty(val)) {
				this._email.val($.cookie("email"));
			}
	
			var renewKaptchaP = $.proxy(this.renewKaptcha, this);
			this._kaptchaImg.click(renewKaptchaP);
			jqe.find("#renewKaptcha").click(renewKaptchaP);
			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));
	
			var resetPwdP = $.proxy(this.resetPwd, this);
			jqe.find("#resetPwdBtn").click(resetPwdP);
	
			jqe.keypress(function(e) {
				if (e.keyCode == '13') {
					resetPwdP();
				}
			});
			this.renewKaptcha();
			this._delayFocus(this._email);
		}, this));
		return false;
	},
	_showActivateLogin : function() {
		this.hideMsg();
		$.when(this._loadFrag('activateLogin')).
		always($.proxy(function() {
			var jqe = this._frags.find('#activateLogin').clone();
			this._showContent('activateLogin', jqe);
			this._kaptchaImg = jqe.find('#kaptchaImg');
			this._kaptcha = jqe.find('#kaptcha');
			this._email = jqe.find("#email");

			this._langChanged();
			
			val = $.QueryString["id"];
			if (val != null) {
				this._email.val(val);
			}
	
			var renewKaptchaP = $.proxy(this.renewKaptcha, this);
			this._kaptchaImg.click(renewKaptchaP);
			jqe.find("#renewKaptcha").click(renewKaptchaP);
			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));
	
			var activateP = $.proxy(this.activate, this);
			jqe.find("#activateBtn").click(activateP);
	
			jqe.keypress(function(e) {
				if (e.keyCode == '13') {
					activateP();
				}
			});
			this.renewKaptcha();
			this._delayFocus(this._kaptcha);
		}, this));
	},
	_showContent : function(id, frag) {
		this.fire("removing-page", this._page);
		this._page = id;
		this.fire("opening-page", this._page);
		this._content.html(frag);
		this.translateElem(this._content, this.getId());
		this.fire("page-opened", this._page);
	},
	_showBadToken : function() {
		this.hideMsg();
		$.when(this._loadFrag('badToken')).
		always($.proxy(function() {
			var jqe = this._frags.find('#badToken').clone();
			this._showContent('badToken', jqe);
		}, this));
	},
	_showSignIn : function() {
		this.hideMsg();
		$.when(this._loadFrag('signIn')).
		always($.proxy(function() {
			var jqe = this._frags.find('#signIn').clone();
			this._showContent('signIn', jqe);
			this._email = jqe.find("#email");
			this._password = jqe.find("#password");
			this._togglePwd(this._password);
			this._rememberMe = jqe.find("#rememberMe");
			var val = $.cookie("rememberMe");
			if (!this.isEmpty(val)) {
				this._rememberMe.attr('checked', true);
				this._email.val($.cookie("email"));
				this._delayFocus(this._password);
			} else {
				this._delayFocus(this._email);
			}

			this._langChanged();
	
			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));
			var signInP = $.proxy(this.signIn, this);
			jqe.find("#signinBtn").click(signInP);
	
			jqe.find('#forgotPwd').click($.proxy(this._showResetPwd, this));
			
			var self = this;
			jqe.find("#yahooOIBtn").click(function(e_) {
				document.location.href = "openId?op=Yahoo&service=" +self._service;
			});
			jqe.find("#googleOIBtn").click(function(e_) {
				document.location.href = "openId?op=Google&service=" +self._service;
			});
			jqe.keypress(function(e) {
				if (e.keyCode == '13') {
					signInP();
				}
			});
		}, this));
	},
	_showSignedIn : function() {
		this.hideMsg();
		$.when(this._loadFrag('signedIn')).
		always($.proxy(function() {
			var jqe = this._frags.find('#signedIn').clone();
			this._showContent('signedIn', jqe);
			jqe.find("#continueBtn").click($.proxy(this.cancel, this));
		}, this));
	},
	_showSignInS2 : function() {
		this.hideMsg();
		$.when(this._loadFrag('signInS2')).
		always($.proxy(function() {
			var jqe = this._frags.find('#signInS2').clone();
			this._showContent('signInS2', jqe);
			this._question = jqe.find("#question");
			this._answer = jqe.find("#answer");

			this._togglePwd(this._answer);

			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));
			var signInP = $.proxy(this.signInS2, this);
			jqe.find("#signinBtn").click(signInP);

			jqe.keypress(function(e) {
				if (e.keyCode == '13') {
					signInP();
				}
			});
			
			var self = this;
			this.mask("Working");
			var sReqId = butor.sso.SsoAjax.getS2Info(function(result) {
				self.unmask();
				if (!result || result['hasError'] || result['reqId'] != sReqId) {
					return;
				}
				if (result['messages'].length > 0 && result['messages'][0].id == 'QUESTIONS_SETUP_REQUIRED') {
					self._showSetQRs();
					self.info(self.tr('QUESTIONS_SETUP_REQUIRED'));
					return;
				}
				if (!result['data'] || butor.Utils.isEmpty(result['data'][0])) {
					self.back();
					return;
				}
				jqe.find('#avatar').attr('src', '/wl?a=' +result['data'][1]).on('load', function() {
					$(this).fadeIn();
				});
				jqe.find('#question').html(result['data'][0]);
				self._delayFocus(jqe.find('#answer'));
			});
		}, this));
	},
	_showSetQRs : function() {
		gaPush('setQRs');
		$.when(this._loadFrag('setQRs')).
		always($.proxy(function() {
			//var _step = step;
			var jqe = this._frags.find('#setQRs').clone();
			this._showContent('setQRs', jqe);

			this._q1 = jqe.find("#q1");
			this._r1 = jqe.find("#r1");
			this._q2 = jqe.find("#q2");
			this._r2 = jqe.find("#r2");
			this._q3 = jqe.find("#q3");
			this._r3 = jqe.find("#r3");

			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));

			jqe.find('#nextAvatar').click($.proxy(function() {
				this._avIndex += 1;
				if (this._avIndex >= this._avatars.length) {
					this._avIndex = 0;
				}
				jqe.find('#avatar').attr('src', '/wl?a=' +this._avatars[this._avIndex].value);
			}, this));
			jqe.find('#prevAvatar').click($.proxy(function() {
				this._avIndex -= 1;
				if (this._avIndex < 0) {
					this._avIndex = this._avatars.length -1;
				}
				jqe.find('#avatar').attr('src', '/wl?a=' +this._avatars[this._avIndex].value);
			}, this));

			this._delayFocus(jqe.find("#q1"));
			jqe.find('select.qSB').change(function() {
				var oq = $(this).parent().find('div.otherQ');
				oq.removeClass('hidden');
				var q = oq.find('input');
				if ($(this).val() == '10') {
					oq.show();
					q.val('');
				} else {
					oq.hide();
					q.val($(this).find('option:selected').text());
				}
			});

			jqe.find('#okBtn').click($.proxy(this._saveQRs, this));

			this._avIndex = -1;
			var self = this;
			var loadQuestions = function() {
				jqe.find('input.otherQ').attr('placeholder', self.tr('Enter your question'));
				self.hideMsg();
				self.mask("Working");
				var dcReqId = self._attrSet.getCodeSets([{sysId:'sso', id:'sign-q', 'lang':self.getLang()},
				{'id':'sign-avatar', 'lang':''}],
					function(result) {
						self.unmask();
						if (!result || result['hasError'] || !result['data'] || result['data'].length != 2 || result['reqId'] != dcReqId) {
							return;
						}
						butor.Utils.fillSelect(jqe.find('#q1SB'), result['data'][0], {'sorted':false});
						butor.Utils.fillSelect(jqe.find('#q2SB'), result['data'][0], {'sorted':false});
						butor.Utils.fillSelect(jqe.find('#q3SB'), result['data'][0], {'sorted':false});
						jqe.find('select.qSB').change();

						self._avatars = result['data'][1];
						if (self._avIndex == -1) {
							jqe.find('#nextAvatar').click();
						}
					});
			}
			loadQuestions();
			// relaod questions if language change
			App.bind('langChanged', loadQuestions);
		}, this));
	},
	_saveQRs : function() {
		var args = {
			'q1':this._q1.val(),
			'r1':this._r1.val(),
			'q2':this._q2.val(),
			'r2':this._r2.val(),
			'q3':this._q3.val(),
			'r3':this._r3.val()
		};
		for (var i=1; i<=3; i+=1) {
			var q = 'q' +i;
			var r = 'r' +i;
			if (butor.Utils.isEmpty(args[q]) || butor.Utils.isEmpty(args[r])) {
				this.error(this.tr('Please fill in all fields'));
				return;
			}
		}
		args['avatar'] = this._avatars[this._avIndex].value;
		this.mask("Working");
		var self = this;
		var sReqId = butor.sso.SsoAjax.setQRs(args, $.proxy(function(result) {
			this.unmask();
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				return;
			}
			this.info('Please Sign In');
			this.back();
		}, this));
	},
	_showManageApiKey : function () {
		this.hideMsg();
		$.when(this._loadFrag('manageApiKey')).
		always($.proxy(function() {
			var jqe = this._frags.find('#manageApiKey').clone();
			this._showContent('manageApiKey', jqe);
			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));
			var noKeyMsg = 'You have no key';
			var self = this;
			
			// display result
			var displayToken = function (result) {
				if (result){
					jqe.find('#key').html(result);
					jqe.find('#removeBtn').show();
				}else{
					jqe.find('#key').html(self.tr(noKeyMsg));
					jqe.find('#removeBtn').hide();
				}
			}
			
			// get
			$.proxy(this.getApi(displayToken), this);
			
			// generate
			jqe.find('#generateBtn').click(function (){
				$.proxy(self.generateApi(displayToken), self);
			});
			
			// remove
			jqe.find('#removeBtn').click(function (){
				$.proxy(self.removeApi(function(result){
					if (result){
						jqe.find('#key').html(self.tr(noKeyMsg));
						jqe.find('#removeBtn').hide();
					}
				}), self);
			});
			
		}, this));
	},
	_showChangePwd : function() {
		this.hideMsg();
		$.when(this._loadFrag('changePwd')).
		always($.proxy(function() {
			var jqe = this._frags.find('#changePwd').clone();
			this._showContent('changePwd', jqe);
			this._kaptchaImg = jqe.find('#kaptchaImg');
			this._kaptcha = jqe.find('#kaptcha');
			this._email = jqe.find("#email");
			this._password = jqe.find("#password");
			this._togglePwd(this._password);
			this._newPassword = jqe.find("#newPassword");
			this._togglePwd(this._newPassword);
			this._newPassword2 = jqe.find("#newPassword2");
			this._togglePwd(this._newPassword2);

			this._langChanged();
			
			var val = $.cookie("rememberMe");
			if (!this.isEmpty(val)) {
				this._email.val($.cookie("email"));
			}

			val = $.QueryString["id"];
			if (val != null) {
				this._email.val(val);
			}

			if (this._token != null) {
				this._email.butor("disable",true);
				jqe.find("label[for='password']").hide();
				jqe.find("#password").hide();
			}

			var renewKaptchaP = $.proxy(this.renewKaptcha, this);
			this._kaptchaImg.click(renewKaptchaP);
			jqe.find("#renewKaptcha").click(renewKaptchaP);
			jqe.find("#cancelBtn").click($.proxy(this.cancel, this));

			var changePwdP = $.proxy(this.changePwd, this);
			jqe.find("#changePwdBtn").click(changePwdP);
			jqe.keypress(function(e) {
				if (e.keyCode == '13') {
					changePwdP();
				}
			});
			this.renewKaptcha();
			this._delayFocus(this._email);
		}, this));
	},
	_setPwdPopover : function() {
		if (this._newPassword) {
			this._newPassword.parent().popover('destroy').popover({'placement':'right', 'trigger':'manual','html':true,
				'content':this.tr('pwdRulesPopoverMsg')})
				.popover('show');
		}
	},
	_showPage : function(page_) {
		if (this._page) {
			this.fire("removing-page", this._page);
		}
		LOGGER.info("showing page ", page_);
		this._page = page_;

		var ts = "?ts=" +(new Date()).getTime();
		$('#_content').empty();
		var self = this;
		$('#_content').load(page_ +ts, null, function(){
			self.translateElem($('#_content'), self.getId());
		});
	},
	getCurPageDef : function() {
		return {'pageId' : this._page};
	},
	renewKaptcha : function() {
		this._kaptchaImg.attr("src", "kaptcha.jpg?_=" +(new Date()).getTime() +"&si=" +App.getSessionId());
		this._kaptcha && this._kaptcha.val('') && this._kaptcha.focus();
	},
	cancel : function() {
		this.back();
	},
	back : function() {
		var url = '/';
		if (!butor.Utils.isEmpty(this._service)) {
			url = unescape(this._service);
		}
		
		document.location.href = url;
	},
	_togglePwd : function(pwdElm) {
		pwdElm.parent().find('.toggle-pwd').click(function() {
			var t = $(this).find('i');
			if (pwdElm.attr('type') == 'password') {
				pwdElm.attr('type', 'text');
				t.removeClass('fa-eye').addClass('fa-eye-slash');
			} else {
				pwdElm.attr('type', 'password');
				t.removeClass('fa-eye-slash').addClass('fa-eye');
			}
		})
	},
	signIn : function() {
		this.hideMsg();
		var email = this._email.val();
		var password = this._password.val();

		if (this.isEmpty(email) ||
			this.isEmpty(password)) {
			this._delayFocus(this._email);
			this.error(this.tr('Please fill in all fields'));
			return;
		}

		if (this._rememberMe.is(':checked')) { 
			$.cookie("email", email, {'expires' : 365});
			$.cookie("rememberMe", "1", {'expires' : 365});
		} else {
			$.removeCookie("email");
			$.removeCookie("rememberMe");
		}

		var self = this;

		this.mask("Working");
		var sReqId = butor.sso.SsoAjax.signInS1({'id':email, 'pwd':password}, function(result) {
			self.unmask();
			if (!AJAX.isSuccessWithData(result, sReqId)) {
				// change pwd if expired
				if (result && result['messages'] && result['messages'].length > 0 && result['messages'][0].id == 'PWD_EXPIRED') {
					// timeout to login step2, back to step1	
					self._showChangePwd();
					self.error(self.tr('PWD_EXPIRED'));
				}

				return;
			}
			var data = result.data[0];
			if (data.twoStepsSignin == false) {
				//skip challenge questions
				$.cookie("sso_id", data.ssoId, {path:"/"});
				self.back();
				return
			}
			self._showSignInS2();
		});

	},
	signInS2 : function() {
		this.hideMsg();
		var q = this._question.html();
		var r = this._answer.val();

		if (this.isEmpty(q) ||
			this.isEmpty(r)) {
			this._delayFocus(this._answer);
			this.error(this.tr('Please fill in all fields'));
			return;
		}

		var self = this;

		this.mask("Working");
		var sReqId = butor.sso.SsoAjax.signInS2({'q':q, 'r':r}, function(result) {
			self.unmask();
			if (!AJAX.isSuccessWithData(result, sReqId)) {
				if (result && result['messages'] && result['messages'].length > 0 && result['messages'][0].id == 'LOGIN_TIMEOUT') {
					// timeout to login step2, back to step1	
					self._showSignIn();
					self.error(self.tr('LOGIN_TIMEOUT'));
					return;
				}

				// an error message is show but we have to refresh the page then the message will disapear
				// so, will show the message again.
				
				// try another question
				self._showSignInS2();
				var err = 'LOGIN_FAILED';
				if (result['messages'].length > 0) {
					err = result['messages'][0].id;
				}
				self.error(self.tr(err));
				return;
			}
			$.cookie("sso_id", result['data'][0], {path:"/"});
			self.back();
		});
	},
	resetPwd : function() {
		this.hideMsg();

		var email = this._email.val();
		var kaptcha = this._kaptcha.val();

		if (this.isEmpty(email) ||
			this.isEmpty(kaptcha)) {
			this._delayFocus(this._email);
			this.error(this.tr('Please fill in all fields'));
			return;
		}

		var self = this;
		var sReqId = butor.sso.SsoAjax.resetPwd({'id':email, 'kaptcha':kaptcha}, function(result) {
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				self.renewKaptcha();
				return;
			}
			alert(self.tr("Login reset. Check your email for instructions"));

			self._showSignIn();
		});
	},
	activate : function() {
		this.hideMsg();

		var email = this._email.val();
		var kaptcha = this._kaptcha.val();

		if (this.isEmpty(email) ||
			this.isEmpty(kaptcha)) {
			this._delayFocus(this._email);
			this.error(this.tr('Please fill in all fields'));
			return;
		}

		var self = this;
		var sReqId = butor.sso.SsoAjax.activate({'id':email, 'kaptcha':kaptcha, 'token':this._token}, function(result) {
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				self.renewKaptcha();
				return;
			}
			alert(self.tr("Login activated. Please try to sign in"));

			self._showSignIn();
		});
	},
	register : function() {
		this.hideMsg();
		var firstName = this._firstName.val();
		var lastName = this._lastName.val();
		var email = this._email.val();
		var kaptcha = this._kaptcha.val();

		if (this.isEmpty(firstName) ||
			this.isEmpty(lastName) ||
			this.isEmpty(email) ||
			this.isEmpty(kaptcha)) {
			this._delayFocus(this._firstName);
			this.error(this.tr('Please fill in all fields'));
			return;
		}
		var self = this;
		var sReqId = butor.sso.SsoAjax.register({'email':email, 
				'kaptcha':kaptcha,
				'firstName':firstName,
				'lastName':lastName}, function(result) {
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				self.renewKaptcha();
				return;
			}
			alert(self.tr("Registration done successfully. You will receive an email shortly to activate your account"));
			self._showSignIn();
		});
	},
	generateApi : function(callback) {
		this.hideMsg();
		var self = this;
		this.mask("Working");
		var sReqId = butor.sso.SsoAjax.generateApi(function(result) {
			self.unmask();
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				return;
			}
			var token = result.data[0];
			if (callback && typeof callback === 'function') {
				callback(self.tr("This is the only time you can see this Key. Keep it in a safe place") +' : ' +token);
			}
			return;
		});
	},
	getApi : function(callback) {
		this.hideMsg();
		var self = this;
		this.mask("Working");
		var sReqId = butor.sso.SsoAjax.getApi(function(result) {
			self.unmask();
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				return;
			}
			var token = result.data[0];
			if (callback && typeof callback === 'function') {
				callback(self.tr("Your API Key is hashed and could not be displayed! If you forget your key then you can generate a new one"));
			}
			return;
		});
	},
	removeApi : function(callback) {
		this.hideMsg();
		var self = this;
		this.mask("Working");
		var sReqId = butor.sso.SsoAjax.removeApi(function(result) {
			self.unmask();
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				return;
			}
			var removed = result.data[0];
			if (callback && typeof callback === 'function') {
				callback(removed);
			}
			return;
		});
	},
	changePwd : function() {
		this.hideMsg();
		var password = this._password.val();
		var newPassword = this._newPassword.val();
		var newPassword2 = this._newPassword2.val();
		var email = this._email.val();
		var kaptcha = this._kaptcha.val();

		if (this.isEmpty(email) ||
			this.isEmpty(kaptcha) ||
			(this.isEmpty(password) && this.isEmpty(this._token)) ||
			this.isEmpty(newPassword) ||
			this.isEmpty(newPassword2)) {
			this._delayFocus(this._email);
			this.error(this.tr('Please fill in all fields'));
			return;
		}
		if (newPassword != newPassword2) {
			this.error(this.tr('New password fields do not match. Please correct.'));
			this._delayFocus(this._newPassword);
			return;
		}
		var self = this;
		var sReqId = butor.sso.SsoAjax.changePwd({id:email, 
				'pwd':password,
				'token':this._token,
				'newPwd':newPassword,
				'newPwdConf':newPassword2,
				'kaptcha':kaptcha}, function(result) {
			if (!result || result['hasError'] || result['reqId'] != sReqId) {
				self.renewKaptcha();
				return;
			}
			if (result['messages'].length > 0 && result['messages'][0].id == 'QUESTIONS_SETUP_REQUIRED') {
				self._showSetQRs();
				self.info(self.tr('QUESTIONS_SETUP_REQUIRED'));
				return;
			}
			//alert(self.tr("Password change done successfully. Please sign in ..."));
			//self._showSignIn();
			self.back();
		});
	},
	_contactUs : function() {
		var url = this._bundle.get('contact-us-url');
		this._showPage(url);
	},
	mailTo : function(rcpts) {
		document.location.href="mailto:" +rcpts;
	}
});

butor.sso.SsoAjax = function() {
	return {
		signInS1 : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'signInS1'},
					[args_], handler_);
		},
		signInS2 : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'signInS2'},
					[args_], handler_);
		},
		setQRs : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'setQRs'},
					[args_], handler_);
		},
		generateApi : function (handler_) {
			return AJAX.call('login.ajax',
					{'streaming' : false, 'service' : 'generateApi'},
					[], handler_);
		},
		getApi : function (handler_) {			
			return AJAX.call('login.ajax',
					{'streaming' : false, 'service' : 'getApi'},
					[], handler_);
		},
		removeApi : function (handler_) {
			return AJAX.call('login.ajax',
					{'streaming' : false, 'service' : 'removeApi'},
					[], handler_);
		},
		changePwd : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'changePwd'},
					[args_], handler_);
		},
		resetPwd : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'resetPwd'},
					[args_], handler_);
		},
		register : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'register'},
					[args_], handler_);
		},
		activate : function (args_, handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'activate'},
					[args_], handler_);
		},
		getUserInfo : function (handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'getUserInfo'},
					[], handler_);
		},
		getS2Info : function (handler_) {
			return AJAX.call('login.ajax', 
					{'streaming':false, 'service':'getS2Info'},
					[], handler_);
		}
	};
}();

butor.sso.bundles = {
	'ID or Email': {'fr' : 'ID ou Email'},
	'Sign in': {'fr' : 'Se connecter'},
	'Reset login': {'fr' : 'Initialiser login'},
	'New password': {'fr' : 'Nouveau mot de passe'},
	'Confirm new password': {'fr' : 'Confirmez nouveau mot de passe'},
	'Type the text you see in the image': {'fr' : 'Écrire le texte que vous voyez dans l\'image'},
	'Remember my ID': {'fr' : 'Se rappeler de mon ID'},
	'Password': {'fr' : 'Mot de passe'},
	'First name': {'fr' : 'Prénom'},
	'Last name': {'fr' : 'Nom'},
	'Register': {'fr' : 'Inscription'},
	'Forgot password': {'fr' : 'Mot de passe oublié'},
	'Forgot password?': {'fr' : 'Mot de passe oublié?'},
	'Password changed succesfully. Please login': {'fr' : 'Votre mot de passe a été changé. SVP vous connecter'},
	'Login reset. Check your email for instructions': {'fr' : 'Votre login a été initialisé. SVP attendre le courriel sur ce sujet et suivre les instructions' },
	'Login activated. Please try to sign in': {'fr' : 'Votre login a été activé. SVP essayer de vous connecter' },
	'Set up your challenge questions' : {'fr' : 'Définir vos questions de sécurité'},
	'Answer': {'fr' : 'Réponse' },
	'Choose image': {'fr' : 'Choisir image' },
	'Enter your question': {'fr' : 'Entrez votre question' },
	'Activate login' : {'fr' : 'Activer login'},
	'Reset password' : {'fr' : 'Initialiser mot de passe'},
	'Generate new API key': {'fr' : 'Générer nouvelle clé API'},
	'Delete API key': {'fr' : 'Supprimer clé API'},
	'You have no key' : {'fr' : 'Vous n\'avez pas de clé'},
	'Change password' : {'fr' : 'Modifier mot de passe'},
	'Change challenge questions' : {'fr' : 'Modifier questions de sécurité'},
	'Contact us': {'fr' : 'Nous rejoindre'},
	'Your API Key is hashed and could not be displayed! If you forget your key then you can generate a new one': {
		'fr' : 'Votre clé API est hashée et ne peut pas être affichée. Si vous l\'avez oublié alors vous pouvez générer une autre'},
	'This is the only time you can see this Key. Keep it in a safe place': {
		'fr' : 'C\'est la seule occasion que vous voyez cette clé. Conservez-la dans un lieu sécure'},

	'RESET_PWD_NOT_SUPPORTED': {'en' : 'Login reset not supported for this type of account', 'fr' : "Reinitialisation du login n'est pas supportée pour ce type de compte"},
	'QUESTIONS_SETUP_REQUIRED': {'en' : 'Please set up your challenge questions', 'fr' : "SVP définir vos questions de sécurité"},
	'QUESTIONS_SETUP_NOT_UNIQUES': {'en' : 'Questions must be unique', 'fr' : "Les questions doivent être uniques"},
	'QUESTIONS_SETUP_MISSING_INFO': {'en' : 'Please fill in all fields', 'fr' : "SVP remplir tout les champs"},
	'QUESTIONS_SETUP_FAILED': {'en' : 'Failed to setup questions', 'fr' : "La mise à jour des questions a échouée"},
	'LOGIN_SUCCESS': {'en': 'Sign in OK', 'fr': 'Identification réussie'},
	'LOGIN_FAILED': {'en': 'Sign in failed', 'fr': 'Identification échouée'},
	'LOGIN_TIMEOUT': {'en': 'Sign in timed out. Please retry', 'fr': 'Identification expirée. SVP réessayer'},
	'ACCOUNT_DISABLED': {'en': 'Your portal login has been disabled', 'fr': 'Votre login au portail a été désactivé'},
	'ACCOUNT_DISABLED_RESET': {'en': 'Your portal login has been disabled. Please reset your login.', 'fr': 'Votre login au portail a été désactivé. SVP réinitialiser votre login.'},
	'CHANGE_PWD_SUCCESS': {'en': 'Password change OK', 'fr': 'Changement du mot de passe réussi'},
	'RESET_PWD_SUCCESS': {'en': 'Login reset OK', 'fr': 'Reinitialisation du login réussie'},
	'CHANGE_PWD_FAILED': {'en': 'Password change failed', 'fr': 'Changement du mot de passe échoué'},
	'CHANGE_PWD_MIN_REQ': {'en': 'Password change failed (min. requirements)', 'fr': 'Changement du mot de passe échoué (exigences min.)'},
	'CHANGE_PWD_RECENT_USED': {'en': 'Password change failed (last {1} passwords)', 'fr': 'Changement du mot de passe échoué ({1} derniers mot de passe)'},
	'BAD_KAPTCHA': {'en': 'Invalid Kaptcha', 'fr': 'Kaptcha invalide'},
	'CHANGE_PWDS_MISMATCH': {'en': 'New passwords mismatch', 'fr': 'Les nouveaux mot de passe ne sont pas identiques'},
	'PWD_EXPIRED': {'en': 'Your password has expired. Please change your password.', 'fr': 'Votre mot de passe est expiré. SVP changer votre mot de passe.'},
	'RESET_PWD_FAILED': {'en': 'Login reset failed', 'fr': 'Reinitialisation du login échouée'},
	'ACTIVATE_ACCOUNT_FAILED': {'en': 'Login activation failed', 'fr': 'Activation de login échouée'},
	'CHANGE_PWD_NOT_SUPPORTED': {'en': 'Password change not supported. Please contact the help desk.', 'fr': "Changement du mot de passe n'est pas supporté. SVP contactez le centre d'assistance."},
	'RESET_PWD_NOT_SUPPORTED': {'en': 'Password reset not supported. Please contact help desk.', 'fr': "Reinitialisation du mot de passe n'est pas supporté. SVP contactez le centre d'assistance."},
	'REGISTRATION_FAILED': {'en': 'Registration failed', 'fr': 'Inscription échouée'},
	'USER_ALREADY_REGISTERED': {'en': 'Already registered', 'fr': 'Déjà enregistré'},
	'REGISTRATION_SUCCESS': {'en': 'Registration OK', 'fr': 'Inscription réussie'},
	'USE_DIFFERENT_PWD': {'en': 'Registration OK', 'fr': 'Inscription réussie'}
};

$(document).ready(function() {
	(function($) {
		$.QueryString = (function(a) {
			if (a == "") return {};
			var b = {};
			for (var i = 0; i < a.length; ++i)
			{
				var p=a[i].split('=');
				if (p.length != 2) continue;
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}
			return b;
		})(window.location.search.substr(1).split('&'))
	})(jQuery);

	App = new butor.sso.App();
	App.setOptions({'headerVisibleAtStartup':true,
		'contentVisibleAtStartup':true,
		'footerVisibleAtStartup':true});
	
	App.addBundle('common', butor.bundles);
	App.addBundle('common', butor.sso.bundles);
	
	// singleton required by whitelabeling loanding to access APP ----------
	App.Bundle = {
		'add' : $.proxy(App.addBundle, App),
		'get' : $.proxy(App._bundle.get, App._bundle),
		'override' : $.proxy(App._bundle.override, App._bundle)
	};
	// ---------------------

	AJAX.setApp(App);

	$.getScript("/wl?j=1").done(function() {
		App.start();
	}).fail(function() {
		App.start();
	});
});
//# sourceURL=butor.sso.js