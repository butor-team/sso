/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.sso = butor.sso || {};
butor.sso.ticket = butor.sso.ticket || function() {
	return {
		host : '',
		onExpire : function() {
			alert('Oh snap! Ticket has expired! Please retry.');
		},
		getTicket : function(handler) {
			if (typeof handler !== 'function') {
				return;
			}
			var url = butor.sso.ticket.host +'/sso/getticket';
			$.ajax({
				url : url,
				cache : false,
				dataType : 'text'
			}).error(function(jqXHR, status) {
				LOGGER.error(jqXHR);
				if (jqXHR.status = 401) {
					location.href = butor.sso.ticket.host +'/sso/?service=' + location.href;
				} else {
					alert('Unknown error!');
				}
			}).success(function(ticket) {
				handler(ticket);
			});
		},
		getIdentity : function(ticket, handler) {
			if (typeof handler !== 'function') {
				return;
			}
			$.ajax({
				url : butor.sso.ticket.host +'/sso/checkticket',
				data : {
					ticket : ticket
				},
				dataType : 'json'
			})
			.error(function(jqXHR, status) {
				butor.sso.log("error:", jqXHR);
				if (jqXHR.status = 401) {
					if (typeof butor.sso.ticket.onExpire === 'function') {
						butor.sso.ticket.onExpire();
					} else {
						alert('Oh snap! Ticket has expired! Please retry.\ndefine butor.sso.ticket.onExpire as a function to handle this case.');
					}
				} else {
					alert('Unknown error!');
				}
			})
			.success(function(userInfo) {
				handler(userInfo);
			});
		}
	};
}();
//# sourceURL=butor.sso.ticket.js